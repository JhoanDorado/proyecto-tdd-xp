var n = 0;
var param = 'nulo';
var chartA, chartB, chartC;
//Componentes 
ChartAnillos = document.getElementById("chartdiv");
ChartBarr = document.getElementById("chartdiv-barra");
Chartline = document.getElementById("chartdiv-line");
var urla = 'http://localhost:4000/api/read/controlApp/' + param;
let urlb = 'http://localhost:4000/api/read/barra';
let urlc = 'http://localhost:4000/api/read/usoApp';

//funciones de dibujos de graficos
if (ChartAnillos != null) {


    am4core.ready(async function() {

        // Themes begin
        am4core.useTheme(am4themes_material);
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        chartA = am4core.create("chartdiv", am4charts.PieChart);

        // Add data

        await read(chartA, urla);

        chartA.innerRadius = am4core.percent(50);
        // Add and configure Series
        var pieSeries = chartA.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "cantidad";
        pieSeries.dataFields.category = "name_app";
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeOpacity = 1;

        // This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;

        chartA.hiddenState.properties.radius = am4core.percent(0);


    });
}
if (ChartBarr != null) {

    am4core.ready(async function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        chartB = am4core.create("chartdiv-barra", am4charts.XYChart);
        chartB.scrollbarX = new am4core.Scrollbar();

        // Add data
        let url = 'http://localhost:4000/api/read/barra';
        await read(chartB, url);


        // Create axes
        var categoryAxis = chartB.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "region";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = 270;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;

        var valueAxis = chartB.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;

        // Create series
        var series = chartB.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "cantidad";
        series.dataFields.categoryX = "region";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;
        series.tooltip.pointerOrientation = "vertical";

        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;

        // on hover, make corner radiuses bigger
        var hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;

        series.columns.template.adapter.add("fill", function(fill, target) {
            return chartB.colors.getIndex(target.dataItem.index * 4);
        });

        // Cursor
        chartB.cursor = new am4charts.XYCursor();

    }); // end am4core.ready()


}

if (Chartline != null) {

    am4core.ready(async function() {

        // Themes begin
        am4core.useTheme(am4themes_material);
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        chartC = am4core.create("chartdiv-line", am4charts.XYChart);

        // Add data
        let url = 'http://localhost:4000/api/read/usoApp';
        await read(chartC, url);
        console.log(chartC.data)

        // Set input format for the dates
        chartC.dateFormatter.inputDateFormat = "yyyy-MM-dd";

        // Create axes
        var dateAxis = chartC.xAxes.push(new am4charts.DateAxis());
        var valueAxis = chartC.yAxes.push(new am4charts.ValueAxis());

        // Create series
        var series = chartC.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "value";
        series.dataFields.dateX = "date";
        series.tooltipText = "uso: {value}"
        series.strokeWidth = 2;
        series.minBulletDistance = 15;

        // Drop-shaped tooltips
        series.tooltip.background.cornerRadius = 20;
        series.tooltip.background.strokeOpacity = 0;
        series.tooltip.pointerOrientation = "vertical";
        series.tooltip.label.minWidth = 40;
        series.tooltip.label.minHeight = 40;
        series.tooltip.label.textAlign = "middle";
        series.tooltip.label.textValign = "middle";

        // Make bullets grow on hover
        var bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.circle.strokeWidth = 2;
        bullet.circle.radius = 4;
        bullet.circle.fill = am4core.color("#fff");

        var bullethover = bullet.states.create("hover");
        bullethover.properties.scale = 2.3;

        // Make a panning cursor
        chartC.cursor = new am4charts.XYCursor();
        chartC.cursor.behavior = "panXY";
        chartC.cursor.xAxis = dateAxis;
        chartC.cursor.snapToSeries = series;

        // Create vertical scrollbar and place it before the value axis
        chartC.scrollbarY = new am4core.Scrollbar();
        chartC.scrollbarY.parent = chartC.leftAxesContainer;
        chartC.scrollbarY.toBack();

        // Create a horizontal scrollbar with previe and place it underneath the date axis
        chartC.scrollbarX = new am4charts.XYChartScrollbar();
        chartC.scrollbarX.series.push(series);
        chartC.scrollbarX.parent = chartC.bottomAxesContainer;

        dateAxis.start = 0.79;
        dateAxis.keepSelection = true;


    });
}
//funciones para renderisar los graficos (automaticamente o con ciudades)
function render() {
    n = n + 1
    console.log(n)
    if (n == 60) {

        read(chartA, urla)
        read(chartB, urlb)
        read(chartC, urlc)
        n = 0;
    }
    setTimeout("render()", 1000)
}

async function renderCiudad() {
    selector = document.getElementById("combox-singup");
    param = selector.value;
    urla = 'http://localhost:4000/api/read/controlApp/' + param;

    await read(chartA, urla);
}

//funcion que trae la data
async function read(chart, url) {
    fetch(url)
        .then(res => res.json())
        .then(datos => mostrar(datos))
        .catch(e => console.log(e))

    const mostrar = (articulos) => {
        chart.data = articulos
    }
}