selector = document.getElementById("combox-singup");
popover = document.getElementById("popover");

if (selector != null) {

    const url = 'https://gist.githubusercontent.com/emamut/6626d3dff58598b624a1/raw/166183f4520c4603987c55498df8d2983703c316/provincias.json'
    fetch(url)
        .then(response => response.json())
        .then(data => { mostrarDatos(data) })
        .catch(err => console.log(err))
    let ciduad = {}
    const mostrarDatos = (data) => {

        for (let i = 1; i < 25; i++) {
            ciduad[i] = data[i].provincia
            var selector = document.getElementById("combox-singup");
            selector.options[i] = new Option(ciduad[i]);
        }
    }
}

if (popover != null) {
    try {
        var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
        var popoverList = popoverTriggerList.map(function(popoverTriggerEl) {
            return new bootstrap.Popover(popoverTriggerEl)
        })

    } catch (error) {
        console.log(error)
    }
}