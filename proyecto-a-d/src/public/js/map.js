selector = document.getElementById("chartdiv");

if (selector != null) {
    probar();
}

function initialMap() {

    try {
        google.charts.load('current', {
            'packages': ['geochart'],
            'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
        });
        google.charts.setOnLoadCallback(drawRegionsMap);

    } catch (error) {
        console.log(error)
    }
}

async function drawRegionsMap() {
    var data = google.visualization.arrayToDataTable(await array);

    var options = {
        sizeAxis: { minValue: 0, maxValue: 100 },
        region: "EC",
        resolution: "provinces",
        colorAxis: {
            //colors: ['rgb(255, 230, 0)', 'rgb(255, 30, 0)']

            colors: ['rgb(255, 186, 121)', 'rgb(180, 41, 87)']
        },
        backgroundColor: 'rgba(228, 235, 241, 0.247)',
        backgroundColorstroke: 'rgb(247, 247, 247)',
        datalessRegionColor: 'rgb(230, 230, 230)',
        textStyle: { color: 'blue', fontSize: 16 },
        widthheight: 700
    };

    var chart = new google.visualization.GeoChart(document.getElementById('geochart-colors'));
    chart.draw(data, options);
};


var array = [
    ['Ciudad', 'Horas', 'Aplicaciones usadas']
]

async function probar() {
    let url = 'http://localhost:4000/api/read/mapa';
    fetch(url)
        .then(res => res.json())
        .then(datos => mostrar(datos))
        .catch(err => console.log(err))

    const mostrar = (app) => {
        app.forEach(element => {
            array.push([element.ciudad, element.horas, element.uso])
            initialMap();

        });
    }
}