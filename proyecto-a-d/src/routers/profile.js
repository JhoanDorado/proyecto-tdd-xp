const express = require('express');
const ruta = express.Router();
const fetch = require("node-fetch");
const { isLoggedIn } = require('../lib/out')
const dbpool = require('../database')

ruta.post('/profile/:id', async(req, res) => {
    await dbpool.query('SELECT * FROM usuario as u , informacion as i WHERE u.pk_user=i.fk_user and u.id = ?', [req.body.id], (err, filas) => {
        if (err) {
            throw err;
        } else {
            res.render('app/profile', { data: filas });
        }
    });

})
ruta.get('/salir', isLoggedIn, (req, res) => {
    req.logOut();
    res.redirect('/login');
})




module.exports = ruta;