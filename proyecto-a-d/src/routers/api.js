const express = require('express');
const ruta = express.Router();

const dbpool = require('../database')

const cors = require('cors')
ruta.use(express.json())
ruta.use(cors())



//rutas para graficos
ruta.get('/', function(req, res) {
    res.send('Ruta de inicio');
});
ruta.get('/read/controlSmart', async(req, res) => {
    await dbpool.query('Select * from control_smart', (err, filas) => {
        if (err) {
            throw err;
        } else {
            res.send(filas);
        }
    });

});

ruta.get('/read/mapa', async(req, res) => {
    const cero = "00:00:00"
    await dbpool.query('SELECT i.ciudad, count(p.name_app)as uso,sum(TIMESTAMPDIFF(hour,time(?),p.time_aplicacion)) AS horas FROM control_app as p, informacion as i where i.ip = p.fk_ip group by i.ciudad', [cero], (err, filas) => {
        if (err) {
            throw err;
        } else {
            res.send(filas);
        }
    });

});
ruta.get('/read/barra', async(req, res) => {
    await dbpool.query('SELECT i.region ,COUNT(p.name_app) AS cantidad from control_app as p, informacion as i  where p.fk_ip = i.ip GROUP BY i.region', (err, filas) => {
        if (err) {
            throw err;
        } else {
            res.send(filas);
        }
    });

});

ruta.get('/read/usoApp', async(req, res) => {
    const formt = '%Y-%m-%d'
    await dbpool.query('Select DATE_FORMAT(fecha,?) as date , count(fk_ip) as value from control_app GROUP BY fecha order by fecha', [formt], (err, filas) => {
        if (err) {
            throw err;
        } else {
            res.send(filas);
        }
    });

});

ruta.get('/read/usarios', async(req, res) => {
    await dbpool.query('Select * from usuario', (err, filas) => {
        if (err) {
            throw err;
        } else {
            res.send(filas);
        }
    });

});

ruta.get('/read/profile/:id', async(req, res) => {
    const { id } = req.params;
    await dbpool.query('SELECT * FROM usuario as u , informacion as i WHERE u.pk_user=i.fk_user and u.id = ?', [id], (err, filas) => {
        if (err) {
            throw err;
        } else {
            res.send(filas);
        }
    });

});

ruta.get('/read/controlApp/:ciudad', async(req, res) => {
    const { ciudad } = req.params;
    if (ciudad == 'nulo') {
        await dbpool.query("SELECT name_app ,COUNT(name_app) AS cantidad from control_app  GROUP BY name_app;", (err, filas) => {
            if (err) {
                throw err;
            } else {
                res.send(filas);
            }
        });
    } else {
        await dbpool.query("SELECT p.name_app ,COUNT(p.name_app) AS cantidad from control_app as p, informacion as i  where p.fk_ip = i.ip and i.ciudad=?  GROUP BY p.name_app;", [ciudad], (err, filas) => {
            if (err) {
                throw err;
            } else {
                res.send(filas);
            }
        });
    }


});
module.exports = ruta;