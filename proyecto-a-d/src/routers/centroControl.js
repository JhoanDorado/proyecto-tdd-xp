const express = require('express');
const ruta = express.Router();
const { isLoggedIn } = require('../lib/out')


const dbpool = require('../database')

ruta.get('/', isLoggedIn, (req, res) => {
    res.render('app/centroControl');
});



module.exports = ruta;