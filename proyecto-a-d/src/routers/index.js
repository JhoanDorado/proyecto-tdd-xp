const express = require('express');
const rutas = express.Router();
const { isNotLoggedIn } = require('../lib/out')

rutas.get('/', isNotLoggedIn, (req, res) => {
    res.render('home/home')
});


module.exports = rutas;