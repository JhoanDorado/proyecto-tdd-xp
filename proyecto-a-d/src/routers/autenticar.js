const express = require('express');
const ruta = express.Router();
const passport = require('passport')
const { isNotLoggedIn } = require('../lib/out')

//ruta y operaciones sigup
ruta.get('/signup', isNotLoggedIn, (req, res) => {
    res.render('auth/signup')
});
ruta.post('/signup', isNotLoggedIn, passport.authenticate('local.signup', {
    successRedirect: '/centroApp',
    failureRedirect: '/signup',
    failureFlash: true
}));



//ruta y operaciones login
ruta.get('/login', isNotLoggedIn, isNotLoggedIn, (req, res) => {
    res.render('auth/login')
});


ruta.post('/login', (req, res, next) => {
    passport.authenticate('local.login', {
        successRedirect: '/centroApp',
        failureRedirect: '/login',
        failureFlash: true
    })(req, res, next);
});


module.exports = ruta;