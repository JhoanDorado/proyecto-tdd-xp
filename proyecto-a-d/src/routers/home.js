const express = require('express');
const ruta = express.Router();
const { isNotLoggedIn } = require('../lib/out')


const dbpool = require('../database')


ruta.get('/', isNotLoggedIn, (req, res) => {
    res.render('home/home')
});

module.exports = ruta;