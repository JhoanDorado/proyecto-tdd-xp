const passport = require('passport');

const LocalStrategy = require('passport-local').Strategy
const dbpool = require('../database');
const helpers = require('../lib/helprs')

passport.use('local.login', new LocalStrategy({
    usernameField: 'gmail',
    passwordField: 'password',
    passReqToCallback: true

}, async(req, gmail, password, done) => {
    const rows = await dbpool.query('select * from usuario where gmail = ?', [gmail]);
    if (rows.length > 0) {
        const usuario = rows[0];
        const value = await helpers.buscarPassword(password, usuario.password);
        if (value) {
            done(null, usuario, req.flash('success', 'Bienvenido ' + usuario.pk_user));
        } else {
            done(null, false, req.flash('message', 'Contraseña / usuario incorrectos'));
        }
    } else {
        return done(null, false, req.flash('message', 'El usuario no existe'))
    }

}));


passport.use('local.signup', new LocalStrategy({
    usernameField: 'pk_user',
    passwordField: 'password',
    passReqToCallback: true
}, async(req, pk_user, password, done) => {
    const region = 'nn';
    const { nombres, apellidos, gmail, ciudad, edad } = req.body
    const rows = await dbpool.query('select * from usuario where gmail = ? or pk_user = ?', [gmail, pk_user]);
    if (rows.length > 0) {
        return done(null, false, req.flash('message', 'Ya existe una cuenta con el usuario o gmail que ingresaste'))
    } else {
        const newUser = {
            pk_user,
            password,
            nombres,
            apellidos,
            gmail
        };
        const newInfo = {
            ciudad,
            region,
            edad,
            fk_user: pk_user
        };
        newUser.password = await helpers.encriptamiento(password);
        const result = await dbpool.query('INSERT INTO usuario set ?', [newUser]);
        await dbpool.query('INSERT INTO informacion set ?', [newInfo]);
        newUser.id = result.insertId;
        return done(null, newUser)
    }

}));



passport.serializeUser((user, done) => {
    done(null, user.id);
});
passport.deserializeUser(async(id, done) => {
    const row = await dbpool.query('SELECT * FROM usuario WHERE ID = ?', [id]);
    done(null, row[0]);
});