const helpers = {};
const bcrypt = require('bcryptjs');
const fetch = require("node-fetch");
helpers.encriptamiento = async(password) => {
    const Salt = await bcrypt.genSalt(10);

    const hash = await bcrypt.hash(password, Salt);
    return hash;
};
helpers.buscarPassword = async(password, savedPassword) => {
    try {
        return await bcrypt.compare(password, savedPassword);

    } catch (error) {
        console.log(error);
    }
};





module.exports = helpers;