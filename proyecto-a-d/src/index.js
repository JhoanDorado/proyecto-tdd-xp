const express = require('express');
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const path = require('path');

const flash = require('connect-flash'); //para el renderizado de mensajes
const sessions = require('express-session'); //para poder tener las sessiones
const mysqlstore = require('express-mysql-session') //para guardar las sessiones en la base de datos
const { database } = require('./keys')

const passport = require('passport');
// iniciar los modulos 

const app = express(); //ejecuta el modulo express y lo guarda en app
require('./lib/passport');
require('../rabbit-mq/consumer');
//app seria mi aplicacion
//configuraciones
app.set('port', process.env.PORT || 4000); //se le agrega un port a la aplicacion

app.set('views', path.join(__dirname, 'views'));

app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    helpers: require('./lib/handlebars.js')
}));
app.set('view engine', '.hbs');

//middlewares funciones que se ejecutan cunado un usuario hace una peticion al servidor
app.use(sessions({
    secret: 'sesionesdigitized',
    resave: false,
    saveUninitialized: false,
    store: new mysqlstore(database)
}));
app.use(flash());
app.use(morgan('dev'));
app.use(express.urlencoded({
    extends: false
}))
app.use(express.json());
app.use(passport.initialize());
app.use(passport.session());




//Variables globales
app.use((req, res, next) => {
    app.locals.success = req.flash('success');
    app.locals.message = req.flash('message');
    app.locals.user = req.user;
    next();
});


//rutas de nuestro app (o sea las pantallas de la aplciacion)
app.use(require('./routers'));
app.use(require('./routers/autenticar'));
app.use(require('./routers/profile'));

//pantallas
app.use('/home', require('./routers/home'));
app.use('/centroApp', require('./routers/centroControl'));
//apis que queremos usar
app.use('/api', require('./routers/api'));



//publicos, codigo que el navegador puede ver
app.use(express.static(path.join(__dirname, 'public')));


//iniciar el servidor
app.listen(app.get('port'), () => {
    console.log('Server en el puerto', app.get('port'));
});
module.exports = app;