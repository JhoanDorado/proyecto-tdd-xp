const request = require('supertest');
const app = require('../src/index')

/*
testear las apis del proyecto
*/


describe('GET /api/controlSmart', () => {
    it('Responde con un contenido json de toda la informacion de los telefonos', done => {
        request(app)
            .get('/api/read/controlSmart')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});
describe('GET /api/read/mapa', () => {
    it('Responde con un contenido json de las ciduades y el uso del celular', done => {
        request(app)
            .get('/api/read/mapa')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});
describe('GET /api/read/barra', () => {
    it('Responde con un contenido json de las regiones y el uso del celular', done => {
        request(app)
            .get('/api/read/barra')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});
describe('GET /api/read/usoApp', () => {
    it('Responde con un contenido json de la fecha y el uso en el dia del celular', done => {
        request(app)
            .get('/api/read/usoApp')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});
describe('GET /api/read/profile/:id', () => {
    it('Responde con un contenido json con la informacion de un usario', done => {
        request(app)
            .get('/api/read/profile/21')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});
describe('GET /api/controlApp/:ciudad', () => {
    it('Responde con un contenido json del nombre de la app y la cantidad de uso', done => {
        request(app)
            .get('/api/read/controlApp/GUAYAS')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});