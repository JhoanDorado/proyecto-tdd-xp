PARA PODER EJECUTAR EL PROYECTO:
1. ingrese el comando npm run dev
2. vaya a un navegador y en la barra de direccionamiento colocar el puerto "localhost:4000"
3. se ejecuta la aplicación

PARA HACER LOS TEST:
1. debe de abrir una nueva terminal integrada en Visual
    1.1 dar click en la pestaña terminal    
    1.2 dar click en nuevo terminal 
2. en la terminal ingrese el comando npm test
3. se desplegará los test que se hicieron

NOTA:   1. cada vez que se hace testeos la aplicación me crea sesiones en la base de datos
            recordar que es una base de datos remota de clever cloud y es limitada
        2. se tiene que ejecutar el test únicamente, no se puede ejecutar la app y el test al mismo tiempo
            por tema de los puertos

SIMULACIÓN RABBIT MQ- CONSUMIDOR Y PUBLICADOR: 
al correr la aplicación en el primer index.js ejecuta la app consumidor.
    para hacer la simulación debe de hacer lo siguiente:
    1.abrir otro terminal integrado en Visual
    2.ingresar el comando node rabbit-mq/publicador.js
    3.se ejecutará una simulación

modulos de node.js usados:
npm i corst mocha supertest amqplib
npm i express express-handlebars express-session mysql express-session-mysql morgan 
 bcryptjs passport passport-local timeago.js connect-flash express-validator nodemon -D



 RECOMENDACIÓN:
 instalar la extensión Material icon Theme para mejor visualización de las carpetas
