-- tabla de usuario -- tabla fuerte
create table usuario(
    pk_user VARCHAR(20) primary key,
    password varchar(20) NOT NULL,
    gmail varchar(40) NOT NULL,
    fullname varchar(100) NOT NULL    
);
)
    
-- tabla de informacion de usuario -- tablas hijas
create table informacion(
    id_informacion int NOT NULL AUTO_INCREMENT primary key,
    ciudad varchar(20) not null,
    ip VARCHAR(20) unique null,
    peso decimal(4, 2),
    edad int not null,
    fk_usuario_in VARCHAR(20) ,
    CONSTRAINT fk_usuario_in FOREIGN KEY (fk_usuario_in) REFERENCES usuario(pk_user)

);
-- tabla de control del celular
create table control_smart(
    fk_ip VARCHAR(20),
    data_time_on time,
    data_time_off time,
    divice VARCHAR(20),
CONSTRAINT fk_ip_smart FOREIGN KEY (fk_ip) REFERENCES informacion(ip)
);
-- tabla de las aplicaciones
create table control_app(
    name_app VARCHAR(20) not null,
    fecha DATE not null,
    time_aplicacion time,
    fk_ip VARCHAR(20),
    CONSTRAINT fk_ip FOREIGN KEY (fk_ip) REFERENCES informacion(ip)
);